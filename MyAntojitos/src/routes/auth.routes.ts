import { Router } from 'express';
import passport from "../config/passport.config";
import { isLoggedIn} from '../middlewares/auth.middleware'

import { 
  getSignup, 
  postSignup, 
  getLogin, 
  postLogin, 
  logout,
  getHome, 
  profileGet,
  profilePost,
  getPedidos,
  detailModal,
  createPedidos,
  mostrarPedidos

} from '../controllers/auth.controller';

const router = Router();

router.get("/signup", getSignup);
router.post("/signup", postSignup);

router.get("/login", getLogin);
router.post("/login", postLogin);

router.get("/logout", logout);

router.get("/home", getHome);

router.get('/product/:id', detailModal);

//router.post('/pedido', createPedidos);

router.get('/pedido/:id', getPedidos);


// Profile
router.get('/profile', isLoggedIn, profileGet);
router.post('/profile', isLoggedIn, profilePost);

//Orders
router.get("/pedido", getPedidos);

// Google auth Routes
router.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email",
    ],
  })
);
router.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/profile",
    failureRedirect: "/login",
  })
);

// facebook auth Routes
router.get("/auth/facebook",passport.authenticate("facebook", {
    scope: ["email"],
  })
);
router.get(
  "/auth/facebook/callback",
  passport.authenticate("facebook", {
    successRedirect: "/profile",
    failureRedirect: "/login",
  })
);



export default router;