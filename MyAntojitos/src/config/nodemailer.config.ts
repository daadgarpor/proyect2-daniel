import fs from 'fs';
import path from 'path';
import hbs from 'handlebars';
import nodemailer from 'nodemailer';

const verificationTemplate = hbs.compile(
    fs.readFileSync(path.join(__dirname, '../views/template-email.hbs'), 'utf8')
);

const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth:{
        user: process.env.EMAIL,
        pass: process.env.PASS
    }
});


export const confirmAccount = async(to: string, endpoint: string)=>{
    return await transporter.sendMail({
        from: "'Daniel' <danielgp@kavaks.com>",
        to,
        subject: "confirm your account",
        html:verificationTemplate({endpoint})
    })
}