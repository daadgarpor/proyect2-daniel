import passport from "passport";
const LocalStrategy = require("passport-local").Strategy;
const googleStrategy = require("passport-google-oauth20").Strategy;
const facebookStrategy = require("passport-facebook").Strategy;
import bcrypt from "bcrypt";
import { User } from "../entities/User.entity";
import { getRepository } from "typeorm";

const verifyCallback = async (email, password, done) => {
  try {
    const user = await getRepository(User).findOne({ email: email });
    if (!user) {
      return done(null, false, { message: "Not user found" });
    }

    if (!user.password) {
      return done(null, false, {
        message:
          "You signup using google, register your email or login with google",
      });
    }
    const isValid = bcrypt.compareSync(password, user.password);

    if (isValid) return done(null, user);
    else return done(null, false, { message: "Incorrect password" });
  } catch (err) {
    done("Hubo un error grave", false, { message: "Not user found" });
    console.log(err);
  }
};

const strategy = new LocalStrategy(
  {
    usernameField: "email",
    passwordField: "password",
  },
  verifyCallback
);

passport.use(strategy);

passport.serializeUser((user: User, cb) => {
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  getRepository(User)
    .findOne(id)
    .then((user) => cb(null, user))
    .catch((error) => cb(error, null));
});

// Google 👇

const googleVerifyCallback = async (_, __, profile, done) => {
  console.log("profile:", profile);
  const user = await getRepository(User)
    .findOne({ googleId: profile.id })
    .catch((err) => done(err));
  if (user) {
    user.photoUrl = profile._json.picture;
    await getRepository(User).save(user);
    done(null, user);
  } else {
    const newUser = getRepository(User).create({
      googleId: profile.id,
      name: profile.displayName,
      email: profile._json.email
    });
    await getRepository(User).save(newUser);
    console.log("New user: ", newUser);
    done(null, newUser);
  }
};

const gStrategy = new googleStrategy(
  {
    clientID: process.env.GOOGLE_ID,
    clientSecret: process.env.GOOGLE_SECRET,
    callbackURL: `${process.env.ENDPOINT}/auth/google/callback`,
  },
  googleVerifyCallback
);

passport.use(gStrategy);

// Facebook 👇

const facebookVerifyCallback = async (_, __, profile, done) => {
  console.log("profile:", profile);
  const user = await getRepository(User)
    .findOne({ facebookId: profile.id })
    .catch((err) => done(err));
  if (user) {
    user.photoUrl = profile._json.picture.data.url;
    console.log("url imagen: ",profile._json.picture.data);
    await getRepository(User).save(user);
    done(null, user);
  } else {
    const newUser = getRepository(User).create({
      facebookId: profile.id,
      name: profile.displayName,
      email: profile._json.email
      
    });
    console.log("url imagen: ",profile._json.picture.data);
    await getRepository(User).save(newUser);
    console.log("New user: ", newUser);
    done(null, newUser);
  }
};

const fStrategy = new facebookStrategy(
  {
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: `${process.env.ENDPOINT}/auth/facebook/callback`,
    profileFields: ["id", "displayName", "photos", "email"],
  },
  facebookVerifyCallback
);

passport.use(fStrategy);

export default passport;
