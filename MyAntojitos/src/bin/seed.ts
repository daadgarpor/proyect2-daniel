import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { Products } from '../entities/Products.entity';

const products = [
    {
        "price": 60,
        "typeProduct": "Hamburguesa",
        "ingredients": [
          "1/2 cup rice vinegar",
          "5 tablespoons honey",
          "1/3 cup soy sauce (such as Silver Swan®)",
          "1/4 cup Asian (toasted) sesame oil",
          "3 tablespoons Asian chili garlic sauce",
          "3 tablespoons minced garlic",
          "salt to taste",
          "8 skinless, boneless chicken thighs"
        ],
        "image": "https://cocina-casera.com/wp-content/uploads/2016/11/hamburguesa-queso-receta.jpg"
      },
      {
        "price": 40,
        "typeProduct": "Hot Dog",
        "ingredients": [
          "3 1/2 pounds boneless pork shoulder, cut into large pieces",
          "1 tablespoon freshly ground black pepper",
          "1 tablespoon kosher salt, or more to taste",
          "2 tablespoons vegetable oil",
          "2 bay leaves",
          "2 teaspoons ground cumin",
          "1 teaspoon dried oregano",
          "1/4 teaspoon cayenne pepper",
          "1 orange, juiced and zested"
        ],
        "image": "https://www.hola.com/imagenes/cocina/noticiaslibros/20200424166483/dia-mundial-perrito-caliente-hot-dog/0-815-495/chicago-hotdog-z.jpg"
      },
      {
        "price": 40,
        "typeProduct": "Niño Envuelto",
        "ingredients": [
          "6 cups grated carrots",
          "1 cup brown sugar",
          "1 cup raisins",
          "4 eggs",
          "1 1/2 cups white sugar",
          "1 cup vegetable oil",
          "2 teaspoons vanilla extract",
          "1 cup crushed pineapple, drained",
          "3 cups all-purpose flour",
          "1 1/2 teaspoons baking soda",
          "1 teaspoon salt",
          "4 teaspoons ground cinnamon"
        ],
        "image": "https://i.ytimg.com/vi/jaLTS6I86BI/maxresdefault.jpg"
      }
];

export const seed = (database_url) => {
  const POSTGRES_URL = database_url
  createConnection({
    type: 'postgres',
    url: POSTGRES_URL,
    entities: ["dist/entities/*.js"],
    synchronize: true
  }).then(async self => {
    console.log(`Is connected?: "${self.isConnected}"`)
    console.log(`Connected to the database with the options: "${self.options}"`);
    await self.synchronize(true); 
    return self;
  })
  .then(async connection => {
    await connection.
    createQueryBuilder()
    .insert()
    .into(Products)
    .values(products)
    .execute()
    console.log("Seeding was successful");
    await connection.close();
    console.log("Is connected?: ", connection.isConnected);
  })
}