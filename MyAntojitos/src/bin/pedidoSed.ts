import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { Products } from '../entities/Products.entity';
import { Pedido } from '../entities/Pedido.entity';

const pedidos = [{
    "cantidad": 3,
    "total": 180,
    "tipoProducto":"Hamburguesa"
    
}
  
];

export const seed = (database_url) => {
  const POSTGRES_URL = database_url
  createConnection({
    type: 'postgres',
    url: POSTGRES_URL,
    entities: ["dist/entities/*.js"],
    synchronize: true
  }).then(async self => {
    console.log(`Is connected?: "${self.isConnected}"`)
    console.log(`Connected to the database with the options: "${self.options}"`);
    await self.synchronize(true); 
    return self;
  })
  .then(async connection => {
    await connection.
    createQueryBuilder()
    .insert()
    .into(Pedido)
    .values(pedidos)
    .execute()
    console.log("Seeding was successful");
    await connection.close();
    console.log("Is connected?: ", connection.isConnected);
  })
}