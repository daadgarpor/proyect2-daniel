import { Request, Response, NextFunction } from "express";
import { User } from "../entities/User.entity";
import { getRepository } from "typeorm";
import passport from "../config/passport.config";
import bcrypt from "bcrypt";
import { confirmAccount } from "../config/nodemailer.config";
import { Products } from "../entities/Products.entity";
import { Pedido } from "../entities/Pedido.entity";
import { Console } from "console";
import { isLoggedIn } from "../middlewares/auth.middleware";

export const getSignup = (req: Request, res: Response, next: NextFunction) => {
  res.render("auth/signup");
};

export const postSignup = (req: Request,  res: Response) =>{
  const { name,email,password } = req.body;
  if (email === '' || password === '') {
      res.render('auth/signup', {errorMessage: "Correo y contraseña Requeridos"})
  }
  //Cliente
if (email === "leinad5@gmail.com" && password=== '123456') {
  res.render('clientePedidos')
} 

  getRepository(User).findOne({email}).then( user =>{
      if(!user){
          const salt = bcrypt.genSaltSync(10);
          const hashPassword = bcrypt.hashSync(password, salt);
          const newUser = getRepository(User).create({ name,email,password:hashPassword})
          getRepository(User).save(newUser).then(()=>{
              res.redirect('/profile');
          }).catch(err => console.log(err))
      }else{
          res.render('auth/signup',{ errorMessage:"Este email ya fue registrado"})
      }
  }).catch(error => console.log(error))
}

export const getLogin = (req: any, res: Response, next: NextFunction) => {
  let message = req.flash("error")[0];
  res.render("auth/login", { message });
};


export const postLogin = passport.authenticate("local", {
  successRedirect: "/profile",
  failureRedirect: "/login",
  failureFlash: true,
});

export const logout = (req: any, res: Response, next: NextFunction) => {
  req.logout();
  req.session.destroy(() => {
    req.logOut();
    res.clearCookie("graphNodeCookie");
    res.status(200);
    res.redirect("/login");
  });
};

export const profileGet = async (req: any, res: Response, next: NextFunction) =>{
  const { user } = req;

  const pedido = await getRepository(Pedido).findOne();
 
  console.log(pedido)

  res.render('profile', {user, pedido});
}

export const profilePost = async (req: any, res: Response, next: NextFunction) =>{

  const { latitude, longitude } = req.params;
  const { user } = req;
  console.log(latitude,longitude)

  try {
    await getRepository(User).update(user.id,{location:{coordinates:[longitude, latitude]}});
    res.redirect("home");
    
  } catch(err) {
    console.log(err)
    res.render('profile',{message:'Guarda tu ubicación para hacer tus pedidos '+ err})
  }
}

export const getHome = async (req: Request, res: Response) => {
  try {
    const product = await getRepository(Products)
      .createQueryBuilder("products")
      .execute();

      //console.log(product);

    res.render("home", { product });
  } catch (err) {
    console.log(err);
  }
};

export const detailModal = async (req: Request, res: Response) => {
  const { id } = req.params;
  const productss = await getRepository(Products).findOne(id);

  console.log(productss)
  res.render('modal', productss)
}


export const getPedidos = async(req: any, res: Response, next: NextFunction) => {


  const { id } = req.params;
  const productss = await getRepository(Products).findOne(id);

  console.log(id);

  let can= 3;
  let tot = can*productss.price;

  const pedidos = getRepository(Pedido).create({cantidad:can,total:tot,precio:productss.price,tipo:productss.typeProduct});

  console.log(pedidos)
  const { cantidad,total,created_at,tipo, precio } = await getRepository(Pedido).save(pedidos);

  res.render("pedidos", {cantidad,total, created_at, tipo, precio});
};

export const createPedidos = async (req: Request, res: Response) => {
  console.log("AKAAAA")

  // res.redirect(`/pedidos/${pedidoId}`);
}


export const mostrarPedidos = async (req: Request, res: Response) => {


 
}


