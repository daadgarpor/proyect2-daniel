import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { User } from './User.entity';
import { Pedido } from './Pedido.entity';

@Entity()
export class Products {
  @PrimaryGeneratedColumn('uuid')
  idProduct: string;

  @Column()
  price: number;

  @Column({
  type: 'enum',
  enum: ['Hamburguesa', 'Hot Dog','Niño Envuelto'],
  default:'Hamburguesa'})
  typeProduct: string;

  @Column("simple-array")
  ingredients: string[];

  @Column({default:"https://images.media-allrecipes.com/images/75131.jpg" })
  image: string;

  @ManyToOne(type => Pedido, pedido => pedido.pedidoId)
  pedido: Pedido;

}