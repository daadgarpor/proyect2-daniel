import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { Products } from './Products.entity';
import { User } from './User.entity';

@Entity()
export class Pedido {

    @PrimaryGeneratedColumn()
    pedidoId: string;

    @Column()
    cantidad: number;

    @Column()
    total: number;

    @Column({ nullable: true })
    precio: number;

    @Column({ nullable: true })
    tipo: string;

    @Column("timestamp", {
        name: "created_at",
        default: (): string => "LOCALTIMESTAMP",
    })
    created_at: Date;

    @ManyToOne(type => User, user => user.id)
    user: User;

    @OneToMany((type) => Products, (products) => products.idProduct)
    products: Products[];
}

