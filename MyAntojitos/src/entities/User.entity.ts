import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { Pedido } from './Pedido.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;
  
  @Column()
  email: string;

  @Column({ nullable: true })
  password: string;

  @Column({ nullable: true })
  googleId: string;

  @Column({ nullable: true })
  facebookId: string;

 @Column("simple-json", {nullable: true})
  location: { coordinates: number[] };

  @OneToMany(type => Pedido, pedido => pedido.pedidoId)
  pedido: Pedido[]; 
}