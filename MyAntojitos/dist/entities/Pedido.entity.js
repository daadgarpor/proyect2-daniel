"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pedido = void 0;
var typeorm_1 = require("typeorm");
var Products_entity_1 = require("./Products.entity");
var User_entity_1 = require("./User.entity");
var Pedido = /** @class */ (function () {
    function Pedido() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", String)
    ], Pedido.prototype, "pedidoId", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], Pedido.prototype, "cantidad", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], Pedido.prototype, "total", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", Number)
    ], Pedido.prototype, "precio", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], Pedido.prototype, "tipo", void 0);
    __decorate([
        typeorm_1.Column("timestamp", {
            name: "created_at",
            default: function () { return "LOCALTIMESTAMP"; },
        }),
        __metadata("design:type", Date)
    ], Pedido.prototype, "created_at", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return User_entity_1.User; }, function (user) { return user.id; }),
        __metadata("design:type", User_entity_1.User)
    ], Pedido.prototype, "user", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Products_entity_1.Products; }, function (products) { return products.idProduct; }),
        __metadata("design:type", Array)
    ], Pedido.prototype, "products", void 0);
    Pedido = __decorate([
        typeorm_1.Entity()
    ], Pedido);
    return Pedido;
}());
exports.Pedido = Pedido;
