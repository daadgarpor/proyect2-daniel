"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Products = void 0;
var typeorm_1 = require("typeorm");
var Pedido_entity_1 = require("./Pedido.entity");
var Products = /** @class */ (function () {
    function Products() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn('uuid'),
        __metadata("design:type", String)
    ], Products.prototype, "idProduct", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], Products.prototype, "price", void 0);
    __decorate([
        typeorm_1.Column({
            type: 'enum',
            enum: ['Hamburguesa', 'Hot Dog', 'Niño Envuelto'],
            default: 'Hamburguesa'
        }),
        __metadata("design:type", String)
    ], Products.prototype, "typeProduct", void 0);
    __decorate([
        typeorm_1.Column("simple-array"),
        __metadata("design:type", Array)
    ], Products.prototype, "ingredients", void 0);
    __decorate([
        typeorm_1.Column({ default: "https://images.media-allrecipes.com/images/75131.jpg" }),
        __metadata("design:type", String)
    ], Products.prototype, "image", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Pedido_entity_1.Pedido; }, function (pedido) { return pedido.pedidoId; }),
        __metadata("design:type", Pedido_entity_1.Pedido)
    ], Products.prototype, "pedido", void 0);
    Products = __decorate([
        typeorm_1.Entity()
    ], Products);
    return Products;
}());
exports.Products = Products;
