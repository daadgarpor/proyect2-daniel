"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var passport_config_1 = __importDefault(require("../config/passport.config"));
var auth_middleware_1 = require("../middlewares/auth.middleware");
var auth_controller_1 = require("../controllers/auth.controller");
var router = express_1.Router();
router.get("/signup", auth_controller_1.getSignup);
router.post("/signup", auth_controller_1.postSignup);
router.get("/login", auth_controller_1.getLogin);
router.post("/login", auth_controller_1.postLogin);
router.get("/logout", auth_controller_1.logout);
router.get("/home", auth_controller_1.getHome);
router.get('/product/:id', auth_controller_1.detailModal);
//router.post('/pedido', createPedidos);
router.get('/pedido/:id', auth_controller_1.getPedidos);
// Profile
router.get('/profile', auth_middleware_1.isLoggedIn, auth_controller_1.profileGet);
router.post('/profile', auth_middleware_1.isLoggedIn, auth_controller_1.profilePost);
//Orders
router.get("/pedido", auth_controller_1.getPedidos);
// Google auth Routes
router.get("/auth/google", passport_config_1.default.authenticate("google", {
    scope: [
        "https://www.googleapis.com/auth/userinfo.profile",
        "https://www.googleapis.com/auth/userinfo.email",
    ],
}));
router.get("/auth/google/callback", passport_config_1.default.authenticate("google", {
    successRedirect: "/profile",
    failureRedirect: "/login",
}));
// facebook auth Routes
router.get("/auth/facebook", passport_config_1.default.authenticate("facebook", {
    scope: ["email"],
}));
router.get("/auth/facebook/callback", passport_config_1.default.authenticate("facebook", {
    successRedirect: "/profile",
    failureRedirect: "/login",
}));
exports.default = router;
