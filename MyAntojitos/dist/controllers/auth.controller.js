"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mostrarPedidos = exports.createPedidos = exports.getPedidos = exports.detailModal = exports.getHome = exports.profilePost = exports.profileGet = exports.logout = exports.postLogin = exports.getLogin = exports.postSignup = exports.getSignup = void 0;
var User_entity_1 = require("../entities/User.entity");
var typeorm_1 = require("typeorm");
var passport_config_1 = __importDefault(require("../config/passport.config"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var Products_entity_1 = require("../entities/Products.entity");
var Pedido_entity_1 = require("../entities/Pedido.entity");
exports.getSignup = function (req, res, next) {
    res.render("auth/signup");
};
exports.postSignup = function (req, res) {
    var _a = req.body, name = _a.name, email = _a.email, password = _a.password;
    if (email === '' || password === '') {
        res.render('auth/signup', { errorMessage: "Correo y contraseña Requeridos" });
    }
    //Cliente
    if (email === "leinad5@gmail.com" && password === '123456') {
        res.render('clientePedidos');
    }
    typeorm_1.getRepository(User_entity_1.User).findOne({ email: email }).then(function (user) {
        if (!user) {
            var salt = bcrypt_1.default.genSaltSync(10);
            var hashPassword = bcrypt_1.default.hashSync(password, salt);
            var newUser = typeorm_1.getRepository(User_entity_1.User).create({ name: name, email: email, password: hashPassword });
            typeorm_1.getRepository(User_entity_1.User).save(newUser).then(function () {
                res.redirect('/profile');
            }).catch(function (err) { return console.log(err); });
        }
        else {
            res.render('auth/signup', { errorMessage: "Este email ya fue registrado" });
        }
    }).catch(function (error) { return console.log(error); });
};
exports.getLogin = function (req, res, next) {
    var message = req.flash("error")[0];
    res.render("auth/login", { message: message });
};
exports.postLogin = passport_config_1.default.authenticate("local", {
    successRedirect: "/profile",
    failureRedirect: "/login",
    failureFlash: true,
});
exports.logout = function (req, res, next) {
    req.logout();
    req.session.destroy(function () {
        req.logOut();
        res.clearCookie("graphNodeCookie");
        res.status(200);
        res.redirect("/login");
    });
};
exports.profileGet = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user, pedido;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                user = req.user;
                return [4 /*yield*/, typeorm_1.getRepository(Pedido_entity_1.Pedido).findOne()];
            case 1:
                pedido = _a.sent();
                console.log(pedido);
                res.render('profile', { user: user, pedido: pedido });
                return [2 /*return*/];
        }
    });
}); };
exports.profilePost = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, latitude, longitude, user, err_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.params, latitude = _a.latitude, longitude = _a.longitude;
                user = req.user;
                console.log(latitude, longitude);
                _b.label = 1;
            case 1:
                _b.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).update(user.id, { location: { coordinates: [longitude, latitude] } })];
            case 2:
                _b.sent();
                res.redirect("home");
                return [3 /*break*/, 4];
            case 3:
                err_1 = _b.sent();
                console.log(err_1);
                res.render('profile', { message: 'Guarda tu ubicación para hacer tus pedidos ' + err_1 });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.getHome = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var product, err_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, typeorm_1.getRepository(Products_entity_1.Products)
                        .createQueryBuilder("products")
                        .execute()];
            case 1:
                product = _a.sent();
                //console.log(product);
                res.render("home", { product: product });
                return [3 /*break*/, 3];
            case 2:
                err_2 = _a.sent();
                console.log(err_2);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.detailModal = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, productss;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                return [4 /*yield*/, typeorm_1.getRepository(Products_entity_1.Products).findOne(id)];
            case 1:
                productss = _a.sent();
                console.log(productss);
                res.render('modal', productss);
                return [2 /*return*/];
        }
    });
}); };
exports.getPedidos = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, productss, can, tot, pedidos, _a, cantidad, total, created_at, tipo, precio;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = req.params.id;
                return [4 /*yield*/, typeorm_1.getRepository(Products_entity_1.Products).findOne(id)];
            case 1:
                productss = _b.sent();
                console.log(id);
                can = 3;
                tot = can * productss.price;
                pedidos = typeorm_1.getRepository(Pedido_entity_1.Pedido).create({ cantidad: can, total: tot, precio: productss.price, tipo: productss.typeProduct });
                console.log(pedidos);
                return [4 /*yield*/, typeorm_1.getRepository(Pedido_entity_1.Pedido).save(pedidos)];
            case 2:
                _a = _b.sent(), cantidad = _a.cantidad, total = _a.total, created_at = _a.created_at, tipo = _a.tipo, precio = _a.precio;
                res.render("pedidos", { cantidad: cantidad, total: total, created_at: created_at, tipo: tipo, precio: precio });
                return [2 /*return*/];
        }
    });
}); };
exports.createPedidos = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        console.log("AKAAAA");
        return [2 /*return*/];
    });
}); };
exports.mostrarPedidos = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/];
    });
}); };
