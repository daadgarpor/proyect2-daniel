"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
require("reflect-metadata");
var typeorm_1 = require("typeorm");
var Products_entity_1 = require("../entities/Products.entity");
var products = [
    {
        "price": 60,
        "typeProduct": "Hamburguesa",
        "ingredients": [
            "1/2 cup rice vinegar",
            "5 tablespoons honey",
            "1/3 cup soy sauce (such as Silver Swan®)",
            "1/4 cup Asian (toasted) sesame oil",
            "3 tablespoons Asian chili garlic sauce",
            "3 tablespoons minced garlic",
            "salt to taste",
            "8 skinless, boneless chicken thighs"
        ],
        "image": "https://cocina-casera.com/wp-content/uploads/2016/11/hamburguesa-queso-receta.jpg"
    },
    {
        "price": 40,
        "typeProduct": "Hot Dog",
        "ingredients": [
            "3 1/2 pounds boneless pork shoulder, cut into large pieces",
            "1 tablespoon freshly ground black pepper",
            "1 tablespoon kosher salt, or more to taste",
            "2 tablespoons vegetable oil",
            "2 bay leaves",
            "2 teaspoons ground cumin",
            "1 teaspoon dried oregano",
            "1/4 teaspoon cayenne pepper",
            "1 orange, juiced and zested"
        ],
        "image": "https://www.hola.com/imagenes/cocina/noticiaslibros/20200424166483/dia-mundial-perrito-caliente-hot-dog/0-815-495/chicago-hotdog-z.jpg"
    },
    {
        "price": 40,
        "typeProduct": "Niño Envuelto",
        "ingredients": [
            "6 cups grated carrots",
            "1 cup brown sugar",
            "1 cup raisins",
            "4 eggs",
            "1 1/2 cups white sugar",
            "1 cup vegetable oil",
            "2 teaspoons vanilla extract",
            "1 cup crushed pineapple, drained",
            "3 cups all-purpose flour",
            "1 1/2 teaspoons baking soda",
            "1 teaspoon salt",
            "4 teaspoons ground cinnamon"
        ],
        "image": "https://i.ytimg.com/vi/jaLTS6I86BI/maxresdefault.jpg"
    }
];
exports.seed = function (database_url) {
    var POSTGRES_URL = database_url;
    typeorm_1.createConnection({
        type: 'postgres',
        url: POSTGRES_URL,
        entities: ["dist/entities/*.js"],
        synchronize: true
    }).then(function (self) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    console.log("Is connected?: \"" + self.isConnected + "\"");
                    console.log("Connected to the database with the options: \"" + self.options + "\"");
                    return [4 /*yield*/, self.synchronize(true)];
                case 1:
                    _a.sent();
                    return [2 /*return*/, self];
            }
        });
    }); })
        .then(function (connection) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, connection.
                        createQueryBuilder()
                        .insert()
                        .into(Products_entity_1.Products)
                        .values(products)
                        .execute()];
                case 1:
                    _a.sent();
                    console.log("Seeding was successful");
                    return [4 /*yield*/, connection.close()];
                case 2:
                    _a.sent();
                    console.log("Is connected?: ", connection.isConnected);
                    return [2 /*return*/];
            }
        });
    }); });
};
