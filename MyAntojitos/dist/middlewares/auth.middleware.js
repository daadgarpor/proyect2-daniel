"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authGlobal = exports.isLoggedIn = void 0;
exports.isLoggedIn = function (req, res, next) {
    console.log("isLoggedIn Middleware");
    req.isAuthenticated() ? next() : res.redirect('/login');
};
exports.authGlobal = function (req, res, next) {
    console.log("IsGLOBLAL");
    req.isAuthenticated() ? req.app.locals.logged = true : req.app.locals.logged = false;
    next();
};
